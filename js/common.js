if($("div").is("#competences_donut")){
    //размер круга
    size = "180px";
    //делим строку, полученную из data-segments в массив (получаем кол-во элементов в круге)
    segments = $("#competences_donut").data("segments").split(';');
    //Общее кол-то поинтов
    points = 0;

    //разбиваем массив segments в двумерный массив, чтобы получить кол-во поинтов, цвет, и напрвавление в каждом сегменте
    for (i = 0; i < segments.length; i++) {
        segments[i] = segments[i].split(',');
        //считаем общее количество поинтов
        points += Number(segments[i][0]);
    }

    //получаем скольки % равен 1 поинт
    point_percent = 100 / points;
    //отступ от предыдущего сегмента
    segment_offset = 100;
    //html кругов
    segments_html = '';

    for (i = 0; i < segments.length; i++) {
        //размер сегмента
        segment_percent = Number(point_percent) * Number(segments[i][0]);
        //% не заполненной части сегмента (не трогать)
        segment_offset_percent = 100 - segment_percent;
        //Собираем все сегменту в 1 html
        segments_html += '<circle data-points="' + segments[i][0] + '" class="donut-segment" cx="21" cy="21" r="40%" fill="transparent" stroke="'+ segments[i][1] +'" stroke-width="8px" stroke-dasharray="'+ segment_percent +' '+ segment_offset_percent +'" stroke-dashoffset="'+ segment_offset +'"></circle>';
        //отступ для следующего сегмента
        segment_offset -= segment_percent;
    }

    //Сборка всего графика в html
    competences_donut_html = '<figure id="donut_basic_compet"><div class="figure-content"><svg width="'+ size +'" height="'+ size +'" viewBox="0 0 42 42" class="donut" aria-labelledby="beers-title beers-desc" role="img"><circle class="donut-ring" cx="21" cy="21" r="40%" fill="transparent" stroke="#d2d3d4" stroke-width="8px" role="presentation"></circle>';
    competences_donut_html += segments_html;
    competences_donut_html += '<g class="chart-text"><text x="50%" y="50%" class="chart-number">'+ points +'</text></g></svg></div></figure>';
    //Вывод графика
    $("#competences_donut").html(competences_donut_html);
    $("#competences_donut").attr("data-segments" , '');
}

$("#donut_basic_compet svg circle").click(function(e) {
    if ($(this).hasClass('active')){
        stroke = $(this).attr("stroke");
        console.log(stroke.substring(0,7));
        $(this).attr("stroke", stroke.substring(0,7));
    }else{
        $('#donut_basic_compet circle').each(function(i,elem) {
            $(this).removeClass('active');
            stroke = $(this).attr("stroke");
            console.log(stroke.substring(0,7));
            $(this).attr("stroke", stroke.substring(0,7));
        });

        $(this).addClass('active');

        stroke = $(this).attr("stroke");

        $(this).attr("stroke", stroke + "80");

        select_trek = $(this).data("points");
        $(".chart-text .chart-number").text(select_trek);
    }
});


$(".open_list_btn").click(function(e) {
    e.preventDefault();
    if ($(this).hasClass('active')) {
        $(".open_list_btn").removeClass('active');
        $(".popup_cloud").removeClass('active');
    }else{
        $(".open_list_btn").removeClass('active');
        $(".popup_cloud").removeClass('active');

        $(this).addClass('active');
        
        className = '.'+$(this).attr('class').split(' ').join('.');
        className = className.replace('.open_list_btn','');
        className = className.replace('.active','');

        $(className + "_popup").addClass('active');
    }
})

$(".like").click(function(e) {
    $(this).toggleClass('active');
})

$(".hobbies_modal_all_list .hobby").click(function(e) {
    parent = $(this).parent();

    $("#" + parent.attr("for") + " + .hobby").toggleClass('active');

    check_cb('.hobbies_modal_all_list');
    
})

$(".interests_modal_all_list .interest").click(function(e) {
    // $(this).toggleClass('active');

    parent = $(this).parent();
    $("#" + parent.attr("for") + " + .interest").toggleClass('active');

    check_cb('.interests_modal_all_list');
})

$(".title_menu").click(function(e) {
    parent = $(this).parent();
    parent.find('.title_menu_content').toggleClass('active');
})




$(".table .del").click(function(e) {
    //выбираем строку, в которой кликнули "удалить"
    parent = $(this).parents(".table-row");

    //плавно скрываем строку
    parent.animate({height: 'hide'}, 300);

    //удаляем строку из html страницы после проигрывания анимации удаления
    setTimeout(function(){
        parent.remove()
    }, 300);
})

$(".trek").click(function(e) {
    if ($(this).hasClass('active')){

    }else{
        $(".trek").removeClass('active');
        $(".trek_content .trek").removeClass('active');
        $(this).addClass('active');

        select_trek = $(this).data("trak-name");
        $(".trek_content .trek.trek-" + select_trek).addClass('active');
    }
})

$(".trek_card").click(function(e) {
    if ($(this).hasClass('active')){
        $(".trek_card").removeClass('active');
        $(".more_info").removeClass('active');
    }else{
        $(".trek_card").removeClass('active');
        $(".more_info").removeClass('active');
        $(this).addClass('active');
        $(this).next(".more_info").addClass('active');
    }
})

$('.type_line').each(function(i,elem) {
    type_points = $(this).data("type");

    blue_line = $(this).find('.blue_line');

    if (type_points < 0) {
        type_points = 50 - (type_points * -1) / 2;
        blue_line.css({'left' : type_points + '%'});
    }else{
        type_points = type_points / 2;
        blue_line.css({'right' : type_points + '%'});
    }
});

$(function(){
    $('.mertors_slider').slick({    
        autoplay: false,
        fade: true,
        cssEase: 'linear'   
    });

    $('.header_photo_gallery').slick({    
        autoplay: false,
        fade: true,
        cssEase: 'linear',
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        cssEase: 'linear',
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.header_photo_gallery',
        focusOnSelect: true,
        centerMode: false,
        arrows: false,
        dots: false
    });

    $(".mertors_slider").on('afterChange', function(event, slick, currentSlide){
        $("#mertors_slider_counter").text(currentSlide + 1);
    });

    $('.party_slider').slick({    
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 9,
        slidesToScroll: 9
    });

    $('.party_slider2').slick({    
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 9,
        slidesToScroll: 9
    });

    $('.line-slider').slick({    
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 3,
        slidesToScroll: 3
    });

    $('.mertors_slider_line').slick({    
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 4,
        slidesToScroll: 4
    });

    $('.line-slider2').slick({    
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 3,
        slidesToScroll: 3
    });

    $('.slider240').slick({
        arrows: true,
        dots: true,    
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 1,
        slidesToScroll: 1
    });
});

function FileName(input){
    $(input).on('change', function(){
        if (this.files.length > 0) {
            $(this).parents('.file').find('label').text($(this)[0].files[0].name);
        }else{
            $(this).parents('.file').find('label').text('Прикрепить обложку');
        }
    });
}

FileName('.form.add_news input[type="file"]');
FileName('.modal_div input[type="file"]');

if($("div").is("#speedometer")){
    //Полная загрузка
    hourly_load = $(".speedometr").data("hourly-load").split(':');

    hour = Number(hourly_load[0]);
    minute = Number(hourly_load[1]);

    rotation_deg = (hour + (minute/60))*(180/24);

    $("#speedometer .line").css({
        transform: 'rotate(' + rotation_deg + 'deg)'
    });

    $("#speedometer .diogram .hourly_load").html("<span>" + hour + "</span>");
}

$(document).ready(function() {
    var overlay = $('#overlay');
    var open_modal = $('.open_modal');
    var close = $('.modal_close, #overlay');
    var modal = $('.modal_div');

    open_modal.click( function(event){
        event.preventDefault();
        var div = $(this).attr('href');
        overlay.fadeIn(200,
            function(){
            $(div).css('display', 'block').animate({opacity: 1, top: '50%'}, 200);
        });
    });

    close.click( function(){ // лoвим клик пo крестику или oверлэю
        modal.animate({opacity: 0, top: '45%'}, 200,
            function(){ // пoсле этoгo
                $(this).css('display', 'none');
                overlay.fadeOut(200); // прячем пoдлoжку
            }
        );
    });
});

$(".sort_date").click(function(e) {
    if ($(this).hasClass('active')){

    }else{
        $(function(){
            var $fields, $container, sorted, index;
          
            $container = $('.news_list_switcher');
            $fields = $("div[data-date]", $container);
            sorted = [];
            $fields.detach().each(function() {
                sorted[parseInt(this.getAttribute("data-date"), 10)] = this;
            });
            sorted.forEach(function(element) {
                $container.append(element);
            });
        });

        $(".sort_views").removeClass('active');
        $(".sort_rate").removeClass('active');
        $(this).addClass('active');
    }
})

$(".sort_rate").click(function(e) {

    if ($(this).hasClass('active')){

    }else{
        $(function(){
            var $fields, $container, sorted, index;
          
            $container = $('.news_list_switcher');
            $fields = $("div[data-rate]", $container);
            sorted = [];
            $fields.detach().each(function() {
                sorted[parseInt(this.getAttribute("data-rate"), 10)] = this;
            });
            sorted.forEach(function(element) {
                $container.append(element);
            });
        });

        $(".sort_date").removeClass('active');
        $(this).addClass('active');
    }
})

$(".toggle_show span").click(function(e) {
    $(".toggle_show").addClass('d-none');
    $(".hide").toggleClass('show');
})

$(".sort_views").click(function(e) {
    if ($(this).hasClass('active')){

    }else{
        $(function(){
            var $fields, $container, sorted, index;
          
            $container = $('.news_list_switcher');
            $fields = $("div[data-views]", $container);
            sorted = [];
            $fields.detach().each(function() {
                sorted[parseInt(this.getAttribute("data-views"), 10)] = this;
            });
            sorted.forEach(function(element) {
                $container.append(element);
            });
        });

        $(".sort_date").removeClass('active');
        $(this).addClass('active');
    }
})

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $("select").selectBoxIt();
})

$(".switch > *").click(function(e) {
    $(this).parent().find('.active').removeClass('active');

    id = $(".switch  > *").index($(this));
    $(this).addClass('active');

    switch_name = $(this).parent().data('switch');

    $('.switch_content[data-switch="' + switch_name + '"] > div').removeClass('active');
    $('.switch_content[data-switch="' + switch_name + '"] > div:eq('+ id +')').addClass('active');
})

function resize() {
    if ($(window).width() <= 1650) {
        $('#left_menu').addClass('expand');
        $('#right_block').addClass('expand roll_up');
        $('.page').addClass('expand_right');
        $('#top_menu').addClass('expand');
    }
    else {
        $('#left_menu').removeClass('expand');
        $('#right_block').removeClass('expand roll_up');
        $('.page').removeClass('expand_right');
        $('#top_menu').removeClass('expand');

        i = 0;
        do {
            setTimeout(function(){
                $('.mertors_slider').slick('refresh');
            }, 10);
            i++;
        } while (i < 15);
    }

    if ($(window).width() < 1340) {
        $('body').addClass('small_screen');
    }
    else {
        $('body').removeClass('small_screen');
    }
}

$(window).resize(resize);
resize();

//сворачиваем правую карточку
$(".roll_up_btn").click(function(e) {
    $('.page').addClass('expand_right');
    $('#right_block').addClass('expand roll_up');
})

//разворачиваем правую карточку
$(".expand_block").click(function(e) {
    $('.page').removeClass('expand_right');
    $('.page').addClass('open_rb');
    $('#right_block').removeClass('expand');
    $('#right_block').addClass('show');

    setTimeout(function(){
        $('.mertors_slider').slick('refresh');
    }, 150);
})

function check_cb(container) {

    checked = false;

    if ($(container + ' .active').length > 0){
        checked = true;
    }else{
        checked = false;
    }

    if (checked == true) {
        $(container + ' .checked_cb_container').show();
    }else{
        $(container + ' .checked_cb_container').hide();
    }
}

check_cb('.hobbies_modal_all_list');
check_cb('.interests_modal_all_list');

$(".add_challenge_images label").click(function(e) {
    modal_id = $(this).attr('href');

    $('#overlay').fadeIn(200,
        function(){
        $(modal_id).css('display', 'block').animate({opacity: 1, top: '50%'}, 200);
    });

    $(".add_challenge_images input").removeAttr("selected");
    $(this).parent().find("input").attr("selected");
})

$(".add_competence").click(function(e) {
    treks = $(this).data('treks');
    treks = treks.split(";");
    options = "";

    for(var i=0; i<treks.length; i++){
        treks[i] = treks[i].split(",");
        options += "<option value='" + treks[i][0] + "'>" + treks[i][1] + "</option>";
    }
    $(".competencies_list .list").append("<div class=\"competence_item\"><div class=\"del\" onclick=\"del_competence_item($(this))\"></div><div class=\"input\"><select name=\"treks[]\" onchange=\"selection_competences($(this));\"><option selected disabled>Трек</option>" + options + "</select></div></div>");
    //<span>Выбрано 2 компетенции</span>

    $("select").selectBoxIt();
})

function del_competence_item(item){
    item.parent().detach();
}
function del_user_item(item){
    parent = item.parents(".user");
    parent.detach();

    list = $(".users-list").val();

    id = parent.data('user_id');
    list = list.replace(id + ", ", "");

    $(".users-list").val(list);

    $("#mentors_list_modal .user[data-user_id=" + id + "]").toggleClass("selected");
}

function select_competences_modal(modal_id){
    $('#overlay').fadeIn(200,
        function(){
        $(modal_id).css('display', 'block').animate({opacity: 1, top: '50%'}, 200);
    });
}

function selection_competences(id) {
    id.parents(".competence_item").find(".l1").detach();
    id.parents(".competence_item").find(".l2").detach();
    id.parents(".competence_item").append("<div class=\"l1\"><span>Профессиональные компетенции <span>(не обязательно)</span></span></div><div class=\"l2 competences_l2_" + id.val() + "\"><span class=\"border_btn\" onclick=\"select_competences_modal('#competences_list_modal_" + id.val() + "')\">Выбрать</span></div>");
}

$("#mentors_list_modal .user").click(function(e) {
    list = $(".users-list").val();
    $(this).toggleClass("selected");

    id = $(this).data('user_id') + ",";

    if ($(this).hasClass("selected")) {
        list += id;
    }else{
        list = list.replace(id, "")
    }

    $(".users-list").val(list);
})

$("#edit_competences .user").click(function(e) {
    $(this).toggleClass("selected");

    id = $(this).data('user_id') + ",";

    if ($(this).hasClass("user_unconfirmed")){
        list_unconfirmed = $(".unconfirmed-list").val();
        if ($(this).hasClass("selected")) {
            list_unconfirmed += id;
        }else{
            list_unconfirmed = list_unconfirmed.replace(id, "");
        }
        
        $(".unconfirmed-list").val(list_unconfirmed);
    }

    if($(this).hasClass("user_member")){
        list_member = $(".member-list").val();
        if ($(this).hasClass("selected")) {
            list_member += id;
        }else{
            list_member = list_member.replace(id, "");
        }
        
        $(".member-list").val(list_member);
    }

    if($(this).hasClass("user_nominated")){
        list_nominated = $(".nominated-list").val();
        if ($(this).hasClass("selected")) {
            list_nominated += id;
        }else{
            list_nominated = list_nominated.replace(id, "");
        }
        
        $(".nominated-list").val(list_nominated);
    }

    if($(this).hasClass("user_winner")){
        list_winner = $(".winner-list").val();
        if ($(this).hasClass("selected")) {
            list_winner += id;
        }else{
            list_winner = list_winner.replace(id, "");
        }
        
        $(".winner-list").val(list_winner);
    }

    if($(this).hasClass("user_ban")){
        list_ban = $(".ban-list").val();
        if ($(this).hasClass("selected")) {
            list_ban += id;
        }else{
            list_ban = list_ban.replace(id, "");
        }
        
        $(".ban-list").val(list_ban);
    }
})

$("#edit_competences .select_all").click(function(e){
    list = "";
    user = $(this).parents("form").find(".user");

    if ($(this).hasClass("unselec")){
        user.removeClass("selected");
        user.each(function(i,elem) {
            list = "";
            return true;
        });
    }else{
        user.addClass("selected");
        user.each(function(i,elem) {
            id = $(this).data('user_id') + ",";
            list += id;
        });
    }

    if (user.hasClass("selected")) {
        $(this).addClass('unselec');
        $(this).val("Отменить выделение");
    }else{
        $(this).removeClass('unselec');
        $(this).val("Выбрать всех");
    }
})

$('.modal_tabs span').each(function(i,elem) {
    new_text = ""
    text = $(this).text();
    text = text.split(" ");
    l_text = text.length;

    text_center = Math.floor(l_text / 2);

    if (l_text > 1) {
        //процерка на четное количество слов в предложении
        if(l_text % 2 == 0){
            for (i = 0; i <= l_text - 1; i++) {
                if (i == text_center-1) {
                    //в центре ставим перенос на новую строка
                    new_text += text[i] + "<br>";
                }else if(i == l_text - 1){
                    //последнее слово без пробела в конце
                    new_text += text[i];
                }else{
                    //слова через пробел
                    new_text += text[i] + " ";
                }
            }
        }else{
            for (i = 0; i <= l_text - 1; i++) {
                if (i == text_center) {
                    //в центре ставим перенос на новую строка
                    new_text += text[i] + "<br>";
                }else if(i == l_text - 1){
                    //последнее слово без пробела в конце
                    new_text += text[i];
                }else{
                    //слова через пробел
                    new_text += text[i] + " ";
                }
            }
        }
    }else{
        new_text = text;
    }

    $(this).html(new_text);
});

$(".competences_list_modal .competences_list .competence_points_card").click(function(e) {
    list = $(".competencies-list").val();
    $(this).toggleClass("not_active");
    $(this).toggleClass("selection");

    id = $(this).data('competences_id');

    if ($(this).hasClass("not_active")) {
        list = list.replace(id + ", ", "")
    }else{
        list += id + ", ";
    }

    $(".competencies-list").val(list);
    count = $('.competence_points_card.selection').length;

    id_trek = $(this).parents(".competences_list_modal").attr('id').replace("competences_list_modal_", "");

    $(".competences_l2_" + id_trek + " .conter").detach();
    if (count > 0) {
        $(".competences_l2_" + id_trek).prepend("<span class=\"conter\">Выбрано элементов компетенции: "+ count +"</span>");
        $(".competences_l2_" + id_trek + " .border_btn").text("Изменить");
    }else{
        $(".competences_l2_" + id_trek + " .border_btn").text("Выбрать");
    }
})

$(".allert").click(function(e) {
    //плавно скрываем строку
    $(this).animate({height: 'hide'}, 300);

    //удаляем строку из html страницы после проигрывания анимации удаления
    setTimeout(function(){
        $(this).detach();
    }, 300);
})

$(".cookie span").click(function(e) {
    //плавно скрываем строку
    $(this).parent().animate({height: 'hide'}, 300);

    //удаляем строку из html страницы после проигрывания анимации удаления
    setTimeout(function(){
        $(this).parent().detach();
    }, 300);
})

file_gallery_clone = $('.file_gallery').clone();

function randomInteger(min, max) {
  // случайное число от min до (max+1)
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

function OnChange_file_gallery(file_gallery_clone){
    id = randomInteger(1, 1000);
    $("#add_gallery_modal .files_uploader").append("<div class=\"file_gallery\"><div class=\"del\" onclick=\"del_competence_item($(this))\"></div><div class=\"input file\"><label class=\"\" for=\"add_gallery_upload_"+ id +"\">Добавить изображение</label><input type=\"file\" id=\"add_gallery_upload_"+ id +"\" name=\"add_gallery_upload[]\" onchange=\"OnChange_FileName(this);\"/></div><div class=\"input\"><textarea name=\"photo_descr[]\" placeholder=\"Описание\"></textarea></div></div>");
}

function OnChange_FileName(this_is){
    if (this_is.files.length > 0) {
        id = $(this_is).attr("id");
        if(this_is.files.length == 1){
            $('label[for = '+ id +']').text($(this_is)[0].files[0].name);
        }else{
            $('label[for = '+ id +']').text("Выбрано фалов: " + this_is.files.length);
        }
    }else{
        $(this_is).parent().find('label').text('Прикрепить файл');
    }
}

$("#top_menu .user").click(function(e) {
    $(this).toggleClass('active');
})


$(".labels_penta div").click(function(e) {
    $(".labels_penta div").removeClass("active");
    $(".treks_popup").addClass("active");
    $(this).addClass("active");
})

var currentElement = $(".roads_list .list .pin.active");
// function currentElement_card(currentElement) {
//     $(".roads_in_sphere .sphere").eq($(currentElement).index()).addClass("active");
// }


$(".roads_list .prev").click(function(e) {
    if (currentElement.prev().length){
        $(".roads_list .list .pin").removeClass('active');
        $(".roads_in_sphere .sphere").removeClass('active');

        currentElement = currentElement.prev();

        currentElement.addClass('active');

        $(".roads_in_sphere .sphere").eq($(currentElement).index()).addClass("active");

        $(".roads_list .next").removeClass('not-active');
        // if (currentElement.prev().length){
        //     $(".roads_list .prev").removeClass('not-active');
        // }else{
        //     $(".roads_list .prev").addClass('not-active');
        // }
    }else{
        $(".roads_list .list .pin").removeClass('active');
        $(".roads_in_sphere .sphere").removeClass('active');

        currentElement = $(".roads_list .list .pin").last();

        currentElement.addClass('active');
        $(".roads_in_sphere .sphere").eq($(currentElement).index()).addClass("active");
    }

})

$(".roads_list .next").click(function(e) {
    if (currentElement.next().length){
        $(".roads_list .list .pin").removeClass('active');
        $(".roads_in_sphere .sphere").removeClass('active');

        currentElement = currentElement.next();

        currentElement.addClass('active');

        $(".roads_in_sphere .sphere").eq($(currentElement).index()).addClass("active");

        $(".roads_list .prev").removeClass('not-active');
        // if (currentElement.next().length){
        //     $(".roads_list .next").removeClass('not-active');
        // }else{
        //     $(".roads_list .next").addClass('not-active');
        // }
    }else{
        $(".roads_list .list .pin").removeClass('active');
        $(".roads_in_sphere .sphere").removeClass('active');

        currentElement = $(".roads_list .list .pin").first();

        currentElement.addClass('active');
        $(".roads_in_sphere .sphere").eq($(currentElement).index()).addClass("active");
    }
})

// $(".roads_list .pin").click(function(e) {
//     $(".roads_list div").removeClass("active");
//     $(".roads_in_sphere .sphere").removeClass("active");
//     $(".treks_popup").addClass("active");
//     $(".roads_in_sphere").addClass("active");
//     $(this).addClass("active");

//     $(".roads_in_sphere .sphere").eq($(this).index()).addClass("active");
// })

$(".treks_name_list span").click(function(e) {
    $(".treks_popup .treks_name_list span").removeClass("active");
    $(this).addClass("active");
    $(".treks_popup").addClass("active");

    $(".roads_in_sphere .sphere > div").removeClass("active");
    $(".roads_in_sphere .sphere > div").eq($(this).index()).addClass("active");

    $(".donut-segment").removeClass("active");
    $(".donut-segment").eq($(this).index()).addClass("active");
})

$(".roads_in_sphere .sphere > div").click(function(e) {
    $(".roads_in_sphere .sphere > div").removeClass("active");
    $(this).addClass("active");
    $(".treks_popup").addClass("active");

    $(".treks_popup .treks_name_list span").removeClass("active");
    $(".treks_popup .treks_name_list span").eq($(this).index()).addClass("active");

    $(".donut-segment").removeClass("active");
    $(".donut-segment").eq($(this).index()).addClass("active");
})

// $(".roads_in_sphere .close").click(function(e) {
//     $(".roads_in_sphere").removeClass("active");
// })

$(".treks_popup .treks_name_list span").click(function(e) {
    $(".treks_popup .treks_name_list span").removeClass("active");
    $(this).addClass("active");

    // $(".roads_in_sphere .sphere > div").removeClass("active");

    // $(".roads_in_sphere .sphere > div").eq(1).addClass("active");
})

$(".treks_popup .close").click(function(e) {
    $(".treks_popup").removeClass("active");
    $(".labels_penta div").removeClass("active");
})

// $(document).mouseup(function (e){ // событие клика по веб-документу
//     var div = $(".treks_popup"); // тут указываем ID элемента
//     if (!div.is(e.target) && div.has(e.target).length === 0) {
//         if ($(".treks_popup").hasClass("active")) {
//             $(".treks_popup").removeClass("active");
//             $(".labels_penta div").removeClass("active");
//         }
//     }
// });

//RADAR
function render_radar(points){

    var marksCanvas = document.getElementById("myChart");

    var marksData = {
      labels: ["", "", "", "", ""],
      datasets: [{
        backgroundColor: "rgba(23, 135, 238, 0.75)",
        pointBackgroundColor: "#ffc500",
        pointRadius: 6,
        pointHoverRadius: 6,
        borderDashOffset: 10,
        borderWidth: "5px",
        data: points,
      }]
    };

    var radarChart = new Chart(marksCanvas, {
      type: 'radar',
      data: marksData,
      options: {
        pointLabel: {
            fontSize: 0
        },
        title : {
            display: false      
        },
        legend : {
            display: false      
        },
        tooltips : {
            enabled: false      
        },
        scale: {
            ticks: {
                maxTicksLimit: 1,
                display: false
            }
        }
      }
    });
}


render_radar([200, 320, 210, 330, 540]);










